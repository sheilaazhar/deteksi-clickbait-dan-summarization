# Deteksi Clickbait dan Summarization pada Artikel Covid-19 dengan Bidirectional Long Short Term Memory dan TF-IDF

## Tahap Penelitian
<img width="333" alt="Alur Penelitian" src="https://user-images.githubusercontent.com/47959466/129434712-e8cc9f2d-0c50-485e-aede-e73b27d56479.png">

### Pengumpulan Data
Pengumpulan data dilakukan dengan cara scraping menggunakan library scrapy di python. Data artikel dikumpulkan dari 4 situs berita yaitu CNN Indonesia, Kompas, Detik dan Kumparan. Data artikel yang diambil hanya judul dan isi konten yang mengandung kata Covid-19 berbahasa Indonesia, yaitu sebanyak 9.440 Data. 

Selanjutnya data tersebut diberi label 0 dan 1. Label 0 untuk artikel clickbait, dan 1 untuk artikel non-clickbait. Lalu data dibagi menjadi 3 jenis, yaitu data latih untuk melatih model, data validasi untuk validasi model serta mencegah overfitting, dan data uji untuk menguji model. Komposisi pembagian data tersebut adalah 70% data latih atau sebesar 6.624 data, 20% data validasi atau sebesar 1.888 data, dan 10% data uji atau sebesar 928 data.

### Preprocessing
Setelah data dikumpulkan, semua data harus dilakukan preprocessing terlebih dahulu untuk dibersihkan agar menjadi lebih terstruktur sehingga data siap untuk dijadikan model dalam penambangan data. Tahap preprocessing yang dilakukan yaitu case folding, cleaning, tokenizing, stopword removal, normalisasi, stemming, dan konversi ke vektor menggunakan Doc2vec.

### Pembuatan Model
Setelah tahap preprocessing dilakukan, data siap untuk dimasukkan ke dalam model Bidirectional LSTM. Model dibuat dengan 2 input yang berbeda, yaitu 1 input untuk judul artikel dan 1 input lainnya untuk konten artikelnya. Output dari kedua input ini akan digunakan untuk model kedepannya. Dan model akan dievaluasi menggunakan confusion matrix. Selanjutnya data isi berita akan di summarize menggunakan algoritma TF-IDF.
Hyperparameter yang digunakan dalam membangun modelnya yaitu menggunakan nilai iterasi (epoch) 100, learning rate 0.001, optimizer-nya Adam, loss function-nya constructive loss, dan batch size 32. Lalu untuk proses peringkasan (summarization) menggunakan algoritma TF-IDF dengan nilai threshold sebesar 1.0 dan 15 skor kalimat tertinggi. 

### Evaluasi
Kemudian, model BiLSTM dievaluasi menggunakan confusion matrix dan diperoleh accuracy 82%, precision 65%, recall 64%, dan f1-score 64%
